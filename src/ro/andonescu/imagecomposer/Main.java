/**
 * 
 */
package ro.andonescu.imagecomposer;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Date;

import javax.imageio.ImageIO;

/**
 * @author iandonescu
 * 
 */
public class Main {

	static String[] facebookProfiles = { "http://graph.facebook.com/100001568142346/picture" };

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		getImage(new URL("file:images/christmas_tree.gif"));
	}

	private static void getImage(URL path) throws IOException {
		BufferedImage image = ImageIO.read(path);
		Graphics2D originalImage = image.createGraphics();

		addQualityToGraphics(originalImage);

		for (String profile : facebookProfiles) {
			BufferedImage profileImage = ImageIO.read(new URL(profile));
			originalImage.drawImage(profileImage, 83, 83, 133, 133, 0, 0, 50,
					50, null);
		}

		originalImage.dispose();
		ImageIO.write(
				image,
				"PNG",
				new File(String.format("images/%s.jpg", new Date().toString()
						.replaceAll("( |:)", "_"))));
	}

	private static void addQualityToGraphics(Graphics2D originalImage) {
		originalImage.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		originalImage.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		originalImage.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

	}
}
